[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.13
[TREE] tree1
((t5:0.276893,t10:0.276893):0.568682,(((t8:0.487697,(t6:0.420027,((t3:0.111531,t2:0.111531):0.244269,t4:0.355800):0.064227):0.067670):0.109288,t7:0.596984):0.210170,(t9:0.326841,t1:0.326841):0.480314):0.038420);
  [treedepth] 0.4
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 31063
[EVOLVE]
  pGTR 1 GTRout  
