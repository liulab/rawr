[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.1
[TREE] tree1
(((t5:0.953947,t3:0.953947):0.884559,(t7:0.776419,(t9:0.668278,(t2:0.148023,t6:0.148023):0.520255):0.108141):1.062086):0.040642,(t1:0.983919,((t8:0.126693,t10:0.126693):0.538184,t4:0.664877):0.319042):0.895228);
  [treedepth] 0.7
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 32559
[EVOLVE]
  pGTR 1 GTRout  
