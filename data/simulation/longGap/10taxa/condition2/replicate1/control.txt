[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.1
[TREE] tree1
(((t1:0.222447,t4:0.222447):1.843727,((t3:0.125241,t8:0.125241):0.352786,(t2:0.344661,((t10:0.173769,t5:0.173769):0.120030,t7:0.293799):0.050861):0.133366):1.588147):0.190364,(t6:2.041201,t9:2.041201):0.215337);
  [treedepth] 0.7
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 15603
[EVOLVE]
  pGTR 1 GTRout  
