[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.1
[TREE] tree1
(((t7:0.491341,t4:0.491341):1.287185,(t5:1.046919,(t9:1.026278,t8:1.026278):0.020641):0.731607):0.436017,((t2:0.018591,t1:0.018591):1.886011,(t10:0.280777,(t3:0.115486,t6:0.115486):0.165291):1.623825):0.309941);
  [treedepth] 0.7
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 15780
[EVOLVE]
  pGTR 1 GTRout  
