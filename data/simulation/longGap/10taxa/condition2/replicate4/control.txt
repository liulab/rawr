[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.1
[TREE] tree1
(((t10:0.749309,t4:0.749309):0.290866,t5:1.040175):1.000279,(((t7:0.188720,(t2:0.053531,t9:0.053531):0.135189):0.412725,t3:0.601445):0.171303,((t1:0.403130,t6:0.403130):0.069988,t8:0.473118):0.299630):1.267707);
  [treedepth] 0.7
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 4101
[EVOLVE]
  pGTR 1 GTRout  
