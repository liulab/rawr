[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.06
[TREE] tree1
(t8:1.455586,((t9:0.145784,t6:0.145784):0.618909,((((t1:0.457901,(t4:0.296818,(t7:0.212587,t5:0.212587):0.084231):0.161084):0.000302,t2:0.458203):0.000519,t3:0.458722):0.259964,t10:0.718686):0.046006):0.690893);
  [treedepth] 1
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 31203
[EVOLVE]
  pGTR 1 GTRout  
