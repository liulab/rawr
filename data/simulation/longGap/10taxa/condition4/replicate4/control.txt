[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.031
[TREE] tree1
((((t6:0.082090,(t9:0.038512,t7:0.038512):0.043578):1.973694,(t10:1.983904,(t4:1.458509,((t3:0.208051,t1:0.208051):0.637025,t2:0.845077):0.613432):0.525395):0.071880):0.103327,t5:2.159111):0.590801,t8:2.749911);
  [treedepth] 1.6
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 4327
[EVOLVE]
  pGTR 1 GTRout  
