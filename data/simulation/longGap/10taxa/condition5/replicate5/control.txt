[TYPE] NUCLEOTIDE 1  
[MODEL]    GTRexample
  [submodel]  GTR 1.26195738509 0.140055369456 0.287783034615 0.35766826674 0.308267431018   //  GTR: a=TtoC, b=TtoA, c=TtoG, d=CtoA, e=CtoG, f=AtoG=1
  [statefreq] 0.311475 0.191363 0.300414 0.196748            //  pi_T=0.1, pi_C=0.2, pi_A=0.3, pi_G=0.4
  [indelmodel]  USER long_gap.txt
  [indelrate] 0.013
[TREE] tree1
((t6:0.038171,t7:0.038171):2.092312,(((t3:0.533129,t10:0.533129):0.740813,t1:1.273942):0.537281,(((t4:0.778132,t8:0.778132):0.272379,(t2:0.038419,t5:0.038419):1.012092):0.319511,t9:1.370022):0.441201):0.319260);
  [treedepth] 4.3
  [branchlengths] NON-ULTRAMETRIC
[PARTITIONS] pGTR  [tree1 GTRexample 1000] //  tree 1, model GTRexample, root length of 1000
[SETTINGS]
  [output] FASTA    // FASTA, NEXUS, PHYLIP or PHYLIPT
  [randomseed] 30385
[EVOLVE]
  pGTR 1 GTRout  
