import simplejson as json
import sys
idfile = 'idMatch_guidance2'
infile = 'guidance2.trees_filxedID'
outfile = 'guidance2.trees'
with open(idfile, 'r') as inf:
    iddict = json.load(inf)

ids = {iddict[x]: x for x in iddict.keys()}
outf = open(outfile, 'w')
with open(infile, 'r') as inf:
    for line in inf.readlines():
        for seqid in ids:
            line = line.replace(seqid, ids[seqid])
        outf.write(line)
outf.close()
